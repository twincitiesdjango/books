# Basic Notes

Starting on 04/15/2015 the group decided to read the book *Lightweight Django* for the next few meetups.

## Book Info

Title: *Lightweight Django*
Authors: Julia Elman & Mark Lavin
Format: Paperback
Publisher: O'Reilly Media, 1st edition (November 13, 2014)
Total Pages: 246
ISBN-13: 978-1-491-94594-0
[Errata](http://www.oreilly.com/catalog/errata.csp?isbn=0636920032502)
[Amazon](http://amzn.com/149194594X) 
[O'Reilly](http://shop.oreilly.com/product/0636920032502.do)
[Github](https://github.com/lightweightdjango/)


# Schedule

1. Meetup 05/20/15 - Read Chapters 1 and 2, total pages ~30
2. Meetup 06/17/15 - 